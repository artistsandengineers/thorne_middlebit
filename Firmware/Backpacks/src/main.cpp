#include <Arduino.h>

#define LED_POWER   5
#define LED_RATE    6
#define LED_BREATH  11
#define LED_PING    13

#define VBATPIN     A7
#define PULSE       A1
#define RESP        A0

#define BIT_1       20
#define BIT_0       21

float breath_filt = 0;
float a = 0.0005;

unsigned long     pulse_duration            = 1000;
unsigned long     prev_time                 = 0;
int               pulse                     = 0;

int               ID                        = 0;

//pulse filler algo.
int               pulse_thresh              = 500;
int               state                     = 0;
unsigned long     prev_change_time          = 0;
int               pulse_max_period          = 1000; //  50  bpm
int               pulse_min_period          = 400;  //  125 bpm...

float             pulse_low_time            = 500;  // need to initialise this otherwise you get an infinity division.
float             pulse_high_time           = 500;  // need to initialise this otherwise you get an infinity division.
float             a_pulse_time             = 0.2;

float             heart_rate                = 60;
float             heart_rate_filtered       = 60;
float             a_heart_rate              = 0.0001;

int               real_pulse                = 0;
int               simulated_pulse           = 0;

int               virtual_pulse_state       = 0;
unsigned long     prev_change_time_virtual  = 0;

//no pulse deteciont, ie. sensor not on person.
unsigned long     no_pulse_time             = pulse_max_period*5;
int               no_pulse                  = 0;

//Serial print timing variables.
unsigned long     serial_print_prev         = 0;
int               print_period              = 50;

//battery stats.
float             measuredvbat              = 0;
float             chargePercentage          = 0;
float             Vbat_Max                  = 4.2;  //max voltage from datasheet.
float             Vbat_Min                  = 3.6;  //0.1V below nominal voltage.

//Serial input.
String            data;
String            trigger                   = "p";
int               isAlive                   = 0;

void readID();
void pulse_filler();
void transmit_data();
void virtual_pulse_generator();
void measureBattery();
void checkSerialData();

void setup() {
  // put your setup code here, to run once:

  Serial1.begin(9600);
  Serial.begin(9600);

  pinMode(LED_POWER, OUTPUT);
  pinMode(LED_BREATH, OUTPUT);
  pinMode(LED_PING, OUTPUT);
  pinMode(LED_RATE, OUTPUT);

  digitalWrite(LED_POWER, HIGH);
  digitalWrite(LED_BREATH, LOW);
  digitalWrite(LED_PING, LOW);
  digitalWrite(LED_RATE, LOW);

  pinMode(BIT_0, INPUT_PULLDOWN);
  pinMode(BIT_1, INPUT_PULLDOWN);

  delay(500);
  readID();

}

void loop() {

  pulse_filler();
  transmit_data();
  measureBattery();
  checkSerialData();

  breath_filt = (1-a)*breath_filt + a*analogRead(RESP);

  //LED's
  analogWrite(LED_BREATH, map(breath_filt,0,1024,0,100));
  digitalWrite(LED_PING, isAlive*255);

}

void readID() {

  ID += digitalRead(BIT_1)<<1;
  ID += digitalRead(BIT_0)<<0;

  ID += 1; //doesn't start from 0;

  Serial.println(ID);

}

void transmit_data() {

  if ((millis() - serial_print_prev) > print_period) {

    //Serial.print(real_pulse);
    //Serial.print("\t");
    //Serial.println(simulated_pulse);
    //Serial.print("\t");
    //Serial.println(heart_rate_filtered);
    //Serial.print("\t");
    //Serial.print(pulse_high_time);
    //Serial.print("\t");
    //Serial.println(pulse_low_time);

    Serial1.print(ID); //ID
    Serial1.print("\t");
    Serial1.print(0); //Packet type - 0 = backpack, 1 = wrist
    Serial1.print("\t");
    Serial1.print(analogRead(RESP)); //Resp
    Serial1.print("\t");
    Serial1.print(breath_filt); //Breath filtered
    Serial1.print("\t");
    Serial1.print(real_pulse*1024);
    Serial1.print("\t");
    Serial1.print(simulated_pulse*1024);
    Serial1.print("\t");
    Serial1.print(measuredvbat);
    Serial1.print("\t");
    Serial1.print(chargePercentage);
    Serial1.println("");

    serial_print_prev = millis();

  }

}

void pulse_filler() {

  //generate virtual pulse based on last real measure
  virtual_pulse_generator();

  int raw = analogRead(PULSE);

  if (raw > pulse_thresh) {

    //detected pulse so not dead..
    no_pulse = 0;

    //GPIO
    digitalWrite(LED_RATE, HIGH);

    real_pulse = 1;
  } else {
    real_pulse = 0;
    digitalWrite(LED_RATE, LOW);
  }

  if ( millis() - prev_change_time > no_pulse_time) {

    //person either dead or not wearing sensor.
    no_pulse = 1;

  }

  if (state == 0) {

    //low to high crossing.
    if (raw > pulse_thresh) {

      Serial.println("rising");

      unsigned long dif = millis() - prev_change_time;
      prev_change_time  = millis();

      //attempt to synch the virtual and real.
      prev_change_time_virtual = millis();

      //check to see its not noise...
      if ((dif < pulse_max_period) && (dif > pulse_min_period)) {

        pulse_low_time = pulse_low_time*(1-a_pulse_time) + a_pulse_time*dif;

      }

      //move to falling state.
      state = 1;
      virtual_pulse_state = 1;
      simulated_pulse = 1;

    }
  }

  if (state == 1) {

    //low to high crossing.
    if (raw < pulse_thresh) {

      Serial.println("falling");

      unsigned long dif = millis() - prev_change_time;
      prev_change_time  = millis();

      //attempt to synch the virtual and real.
      prev_change_time_virtual = millis();

      //check to see its not noise...
      if ((dif < pulse_max_period) && (dif > pulse_min_period)) {

        pulse_high_time = pulse_high_time*(1-a_pulse_time) + a_pulse_time*dif;


      }

      //move to falling state.
      state = 0;
      virtual_pulse_state = 0;
      simulated_pulse = 0;

    }
  }

  heart_rate = (60*1000)/float(pulse_high_time + pulse_low_time);
  heart_rate_filtered = heart_rate_filtered*(1-a_heart_rate)+ a_heart_rate*heart_rate;
  //Serial.println(heart_rate_filtered);

}


void virtual_pulse_generator() {

  //generate virtual pulse based on last real measure
  if (virtual_pulse_state == 0) {

    if (millis() - prev_change_time_virtual > pulse_low_time + 10) {

      if (no_pulse == 0) {
        simulated_pulse = 1;
      } else {
        simulated_pulse = 0;
      }

      prev_change_time_virtual = millis();
      //move to falling state.
      virtual_pulse_state = 1;
    }

  }

  if (virtual_pulse_state == 1) {

    if (millis() - prev_change_time_virtual > pulse_high_time + 10) {

      if (no_pulse == 0) {
        simulated_pulse = 0;
      }

      prev_change_time_virtual = millis();
      //move to falling state.
      virtual_pulse_state = 0;
    }
  }
}

void measureBattery() {

  measuredvbat = analogRead(VBATPIN);
  measuredvbat *= 2;    // we divided by 2, so multiply back
  measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
  measuredvbat /= 1024; // convert to voltage

  chargePercentage = 100*(measuredvbat - Vbat_Min)/(Vbat_Max - Vbat_Min);
  //Serial.print("VBat: " ); Serial.println(measuredvbat);

}

void checkSerialData() {

  data = "";

  while(Serial1.available()>0) {
    data = Serial1.readStringUntil('\n');
  }

  if (data.equals("p")) {
    isAlive = (isAlive+1)%2;
  }

}
