#include <Arduino.h>

#define LED_POWER   5
#define LED_GSR     6
#define LED_PING    10
#define GSR         A1

#define BIT_3       10
#define BIT_2       11
#define BIT_1       12
#define BIT_0       13

#define VBATPIN     A7

//Serial print timing variables.
unsigned long     serial_print_prev         = 0;
int               print_period              = 50;
int               ID                        = 0;

//battery stats.
float             measuredvbat              = 0;
float             chargePercentage          = 0;
float             Vbat_Max                  = 4.2;  //max voltage from datasheet.
float             Vbat_Min                  = 3.6;  //0.1V below nominal voltage.

void readID();
void testLED();
void transmit_data();
void measureBattery();

void setup() {
  // put your setup code here, to run once:

  Serial1.begin(9600);
  Serial.begin(9600);

  pinMode(LED_POWER, OUTPUT);
  pinMode(LED_GSR, OUTPUT);
  pinMode(LED_PING, OUTPUT);

  pinMode(BIT_0, INPUT_PULLDOWN);
  pinMode(BIT_1, INPUT_PULLDOWN);
  pinMode(BIT_2, INPUT_PULLDOWN);
  pinMode(BIT_3, INPUT_PULLDOWN);



  digitalWrite(LED_POWER, HIGH);
  ///igitalWrite(LED_PING, LOW);
  //digitalWrite(LED_RATE, LOW);

  delay(500);
  readID();

}

void loop() {
  // put your main code here, to run repeatedly:

  //testLED();
  transmit_data();
  measureBattery();

  //Serial.println(ID);

}

void readID() {

  ID =  digitalRead(BIT_3)<<3;
  ID += digitalRead(BIT_2)<<2;
  ID += digitalRead(BIT_1)<<1;
  ID += digitalRead(BIT_0)<<0;

  ID += 1; //doesn't start from 0;

  Serial.println(ID);

}

void testLED() {

  digitalWrite(LED_POWER, HIGH);
  digitalWrite(LED_PING, LOW);
  digitalWrite(LED_GSR, LOW);

  delay(100);

  digitalWrite(LED_POWER, LOW);
  digitalWrite(LED_PING, HIGH);
  digitalWrite(LED_GSR, LOW);

  delay(100);

  digitalWrite(LED_POWER, LOW);
  digitalWrite(LED_PING, LOW);
  digitalWrite(LED_GSR, HIGH);

  delay(100);



}


void measureBattery() {

  measuredvbat = analogRead(VBATPIN);
  measuredvbat *= 2;    // we divided by 2, so multiply back
  measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
  measuredvbat /= 1024; // convert to voltage

  chargePercentage = 100*(measuredvbat - Vbat_Min)/(Vbat_Max - Vbat_Min);
  //Serial.print("VBat: " ); Serial.println(measuredvbat);

}

void transmit_data() {

  if ((millis() - serial_print_prev) > print_period) {
    Serial1.print(ID); //Device ID
    Serial1.print("\t");
    Serial1.print(1); //Packet Type (0 = Backpack, 1 = GSR)
    Serial1.print('\t');
    Serial1.print(analogRead(GSR)); //
    Serial1.print("\t");
    Serial1.print("\t");
    Serial1.print(measuredvbat);
    Serial1.print("\t");
    Serial1.print(chargePercentage);
    Serial1.println("");

    serial_print_prev = millis();
  }
}
