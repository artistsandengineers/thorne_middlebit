const fs = require('fs');
const path = require('path');
const express = require('express');
const app =  express();
const http = require('http').createServer(app);
const ws = require('ws');
const nconf = require('nconf');
const serialport = require('serialport');
const serialreadline = require('@serialport/parser-readline');
const datetime = require('node-datetime');

/*Config*/
const localConfigFile = 'config_local.json';
if (! fs.existsSync(localConfigFile)) {
    let defaultConfig = require('./config_defaults.json');
    fs.writeFileSync(localConfigFile, JSON.stringify(defaultConfig, null, 2));
}

nconf.argv().env().file({ file: localConfigFile});

/*Recording and Playback*/
var recording = false;
var playing = false;
var captureFileName = "";
var captureFileStream = null;
var playbackSamples = null;
var recordTimeoutTimer = 0;
var samplePlayhead = 0;
var playbackTimer = 0;

function startRecording() {
    if (!recording) {
        samplePlayhead = 0;
        let captureDirectory = path.join(__dirname, 'captures');
        captureFileName = path.join(captureDirectory, datetime.create().format('Ymd_HMS.txt'));
        if (! fs.existsSync(captureDirectory)) {
            fs.mkdirSync(captureDirectory, { recursive: true });
        }
        captureFileStream = fs.createWriteStream(captureFileName);

        recording = true;
        recordTimeoutTimer = setTimeout(function() {
            stopCaptureAction();
        }, nconf.get('capture:dataCaptureMaxDurationSeconds') * 1000);

        console.log("Capture started to " + captureFileName);
    } else {
        console.log("Not starting capture because one is already active.");
    }
}

function stopCaptureAction() {
    if (recording) {
        clearTimeout(recordTimeoutTimer);
        recording = false;
        captureFileStream.close();
        console.log("Data capture stopped.");
        broadcastCaptureStatusPacket();
        broadcastAvailableCaptureFiles();
    }
    if (playing) {
        clearTimeout(playbackTimer);
        playing = false;
        broadcastCaptureStatusPacket();
        startSerialPorts();
    }
}

function startPlayback(filename) {
    if (recording) {
        console.log("Not starting playback because capture is active.");
        return;
    }

    stopSerialPorts();

    captureFileName = path.join(__dirname, 'captures', filename);
    console.log("Starting playback of " + captureFileName);

    //TODO: This is an exceptionally shit way of doing this but it appears to be working well enough for our immediate purposes.
    playbackSamples = fs.readFileSync(captureFileName, 'utf-8');
    playbackSamples = playbackSamples.split('\r');
    samplePlayhead = 0;
    playing = true;
    playbackTimer = setTimeout(playbackTimerTicked, 0);
}

function playbackTimerTicked() {
    if (samplePlayhead >= playbackSamples.length - 1) {
        samplePlayhead = 0;
    }

    let time = parseInt(playbackSamples[samplePlayhead + 1].split('\t')[0]);
    let line = playbackSamples[samplePlayhead].split(/\t(.+)/)[1]; //discard time portion of line
    broadcastDataToWSClientsWithAddress(inputDataLineToObject(line), '/sensors');
    broadcastCaptureStatusPacket();
    samplePlayhead++;

    playbackTimer = setTimeout(playbackTimerTicked, time);
}

function broadcastAvailableCaptureFiles() {
    let files = fs.readdirSync(path.join(__dirname, 'captures')).filter(function(f) {
        return path.extname(f).toLowerCase() === '.txt'
    });
    broadcastDataToWSClientsWithAddress({ files: files }, '/capture/available');
}

function broadcastCaptureStatusPacket() {
    let o = {};
    o.capturing = recording;
    o.playing = playing;
    o.samplePosition = samplePlayhead;
    o.filename = captureFileName;
    broadcastDataToWSClientsWithAddress(o, '/capture/state');
}

var wristSensorBackpackLUT = nconf.get('wristSensorToBackpackPatch');
console.log("Found " + wristSensorBackpackLUT.length + " backpacks in wrist sensor <-> backpack LUT");
wristSensorBackpackLUT.forEach(function(element) {
    element.lastGSRValue = 0;
    element.batteryVoltage = 0;
    element.batteryPercentage = 0;
    element.lastBreathFudge = 0;
});

function inputDataLineToObject(line) {
    let bits = line.replace('\r', '').split('\t');
    let o = {};
    o.id = parseInt(bits[0]);
    let packetType = parseInt(bits[1]);

    if (packetType === 0) {
        o.respRaw = (parseFloat(bits[2]) / 1024) - 0.5 ;
        o.respLowPass = (parseFloat(bits[3]) / 1024) - 0.5;
        o.pulseRaw = parseFloat(bits[4]) / 1024;
        o.pulseSimulated = (parseFloat(bits[5]) / 1024);
        o.batteryVoltage = parseFloat(bits[6]);
        o.batteryPercentage = parseFloat(bits[7]) / 100;

        wristSensorBackpackLUT.forEach(function(element) {
            if (o.id === element.backpackID) {
                o.gsrRaw = element.lastGSRValue;
                o.wristBatteryValue = element.batteryVoltage;
                o.wristBatteryPercentage = element.batteryPercentage;

                let a = 0.1;
                element.lastBreathFudge = (1 - a) * element.lastBreathFudge + (a * (Math.abs(o.respRaw - o.respLowPass) * 4));
                o.somewhatMadeUpMeasureOfBreathing = element.lastBreathFudge;
            }
        });

        return o;
    } else if (packetType === 1) {
        wristSensorBackpackLUT.forEach(function(element) {
            if (element.wristSensorID === o.id) {
                element.lastGSRValue = parseFloat(bits[2])/ 1024;
                element.batteryVoltage = parseFloat(bits[4]);
                element.batteryPercentage = parseFloat(bits[5]) / 100;
            }
        });
    } else {
        console.log("WARN: Recieved unknown packet type: " + packetType);
    }

    return null;
}

/*Being a Webserver (with ws)*/
app.get('/', function(req, res){
    res.sendFile(path.join(__dirname, 'index.html'));
});
app.use('/chartjs', express.static(__dirname + '/node_modules/chart.js/dist/'));
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/styles.css', express.static(__dirname + '/styles.css'));

http.listen(nconf.get('server:listenPort'), function(){
    console.log('listening on *:3000');
});

const wss = new ws.Server({ server: http });

wss.on('connection', function(socket, req) {
    console.log("ws: New connection from " + req.connection.remoteAddress);

    broadcastAvailableCaptureFiles();
    broadcastCaptureStatusPacket();
    socket.send(JSON.stringify({ address: '/init/backpacks', data : wristSensorBackpackLUT }));

    socket.on('message', function(data) {
        let d = JSON.parse(data);
        if (d.address === '/capture/start') {
            startRecording();
        } else if (d.address === '/capture/stop') {
            stopCaptureAction();
        } else if (d.address === '/capture/play') {
            startPlayback(d.data.file);
        }
    });
});

function broadcastDataToWSClientsWithAddress(data, address) {
    let o = {};
    o.address = address;
    o.data = data;
    let j = JSON.stringify(o, null, 2);
    wss.clients.forEach(function each (client) {
        client.send(j);
    });
}

/*Serial Ports*/
var serialPorts = nconf.get('serial:serialPorts');
console.log('Attempting to do business with the following serial ports:');
console.log(serialPorts);

var lastTimeMillis = 0;
var firstLine = true;
function startSerialPorts() {
    serialPorts.forEach(function (element) {
        element.port = new serialport(element.path, { baudRate: element.baudRate }, function(err) {
            if (err) {
                console.log(err);
                if (nconf.get('serial:crapOutOnSerialPortOpenFailure')) {
                    process.exit(1);
                } else {
                    console.log("Not crashing out because config crapOutOnSerialPortOpenFailure is false.");
                }
            }
        });
        element.parser = new serialreadline();
        element.port.pipe(element.parser);
        element.parser.on('data', function(line) {
            if (firstLine) {
                firstLine = false;
                return;
            }

            if (recording) {
                let time = new Date().getTime();
                captureFileStream.write(time - lastTimeMillis + "\t" + line);
                samplePlayhead++;
                lastTimeMillis = time;
            }

            let a = inputDataLineToObject(line);
            if (a != null) {
                try {
                    broadcastDataToWSClientsWithAddress(inputDataLineToObject(line), "/sensors");
                    if (recording) broadcastCaptureStatusPacket();
                } catch (e) {
                    console.log("Failed to parse input");
                }
            }
        });
    });
}

function stopSerialPorts() {
    serialPorts.forEach(function(element) {
        element.port.close(function(err){});
    });
    console.log("Closed serial ports.");
}

startSerialPorts();
