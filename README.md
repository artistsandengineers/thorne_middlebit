#What
Dumb interface between Sean's clever sensors and Active Theory's clever
renderers.

#IP Addresses and the Like

* Start: 10.0.0.0
* Subnet: /24
* Gateway/DNS: 10.0.0.254

| Device                 | IP                 |
|------------------------|--------------------|
|Renderer Main           |10.0.0.1            |
|Renderer Spare          |10.0.0.2            |
|MiddleBit Main          |10.0.0.3            |
|MiddleBit Spare         |10.0.0.4            |
|Audio Playback Main     |10.0.0.5            |
|Audio Playback Spare    |10.0.0.6            |



#Dependencies

* Windows 10 Pro.
* Built against [Node 10.15.3](https://nodejs.org/en/) but probably anything
  10. will work.
  
#Using it

* Clone this repo.
* `cd MiddleBit`
* `npm install`
* `node index.js`
* Visit `http://localhost:3000` in a browser.
 
At first launch, MiddleBit creates `config_local.json` (which is .gitignored in this repo).
In particular you'll probably need to modify the `serialPorts` section to reflect whatever paths
Windows has assigned to your USBSerial devices.  If you don't have any serial devices because 
we haven't sent you any yet then that's also fine - MiddleBit will operate in 'playback only'
mode so long as `crapOutOnSerialPortOpenFailure` is `false`.

#Recording Data

* Press the record button.

Output files are written to `$repo/MiddleBit/captures/`. Each line of the file represents a
sample from a backpack. Data structure is (tab separated): 

`delta-time-ms pack-id respiration-raw respiration-low-pass heart-raw heart-synthesised`

#Playing Data
* Select a capture in the dropdown
* Press play

Playback loops once it hits the end of an input file.

#Consuming Data (Websockets)
'Subscribed' Websockets clients will receive json objects like this:
```json
{
  "address" : "/foo",
  "data" : "stuff"
}
```

The most interesting of these is the sensor data, which (as of now) looks something like:

```json
{
    "address" : "/sensors",
    "data" : {
      "id" : 0,
      "pulseRaw" : 1.0,
      "pulseSimulated" : 1.0,
      "respRaw" : 1.0,
      "respLowPass" : 1.0,
      "batteryVoltage" : 3.7,
      "batteryPercentage" : 0.8
    }
}
```

...where:

* `id` is the unique ID of the pack that generated the data
* `pulseRaw` is the unprocessed(ish) data from the heart rate ear clip.
* `pulseSimulated` is an attempt at compensating (by filling in the gaps) for the fact that the heart rate ear clip sensor will occasionally suffer dropout.
* `respRaw` is the raw data from the respiration sensor (swings -0.5 to +0.5), rising edges are inhaling, falling is exhaling
* `respLowPass` is a low pass filtered respiration signal
* `batteryVoltage` is the battery voltage in volts
* `batteryPercentage` is a not particularly accurate but good enough for our
  purposes attempt to calculate remaining battery charge in a unit that normal
  people understand.


